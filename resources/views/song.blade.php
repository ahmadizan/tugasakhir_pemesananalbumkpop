@include('template.header')

<section id="band">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 col-lg-4 py-5">
        <div class="card w-100">
          <div class="card-body">
            <h5 class="card-title">Tambah Lagu</h5>

            <form action="/song" method="post">
              @csrf
              <div class="mb-3">
                <label for="name" class="form-label">Judul</label>
                <input type="text" class="form-control" name="name" id="name">
              </div>
              <div class="mb-3">
                <label for="album" class="form-label">Album</label>
                <select class="form-select" id="album" name="album">
                  <option selected>Open this select menu</option>
                  @foreach ($album as $a)
                  <option value={{$a['id']}}>{{$a['name']}}</option>
                  @endforeach
                </select>
              </div>
              <div class="mb-3">
                <label for="group" class="form-label">Group</label>
                <select class="form-select" id="group" name="group">
                  <option selected>Open this select menu</option>
                  @foreach ($group as $b)
                  <option value={{$b['id']}}>{{$b['name']}}</option>
                  @endforeach
                </select>
              </div>
              <button type="submit" class="btn btn-success">Tambah</button>
            </form>

          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div class="w-100 overflow-auto">

          <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Group</th>
                <th scope="col">Album</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @php
              $i = 0
              @endphp
              @foreach ($data as $d)
              @php
              $i += 1
              @endphp
              <tr>
                <td scope="row">{{$i}}</td>
                <td>{{$d->title}}</td>
                <td>{{$d->band->name}}</td>
                <td>{{$d->album->name}}</td>
                <td>
                  <a href="/song/edit/{{$d->id}}" class="btn btn-info">Edit</a>
                  <a href="/song/delete/{{$d->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@include('template.footer')