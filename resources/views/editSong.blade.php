@include('template.header')

<section id="band">
  <div class="container">
    <div class="row py-5">
      <div class="col-12 col-md-6 col-lg-4">
        <div class="card">
          <div class="card-body">
            <h5>Edit Lagu</h5>
            <form action="/song/update/{{$data->id}}" method="post">
              @csrf
              <div class="mb-3">
                <label for="name" class="form-label">Judul</label>
                <input type="text" class="form-control" name="name" id="name" value={{$data->title}}>
              </div>
              <div class="mb-3">
                <label for="album" class="form-label">Album</label>
                <select class="form-select" id="album" name="album">
                  <option selected>Open this select menu</option>
                  @foreach ($album as $a)
                  <option value={{$a['id']}} {{$a['id']==$data->album_id ? 'selected':''}}>{{$a['name']}}</option>
                  @endforeach
                </select>
              </div>
              <div class="mb-3">
                <label for="group" class="form-label">Group</label>
                <select class="form-select" id="group" name="group">
                  <option selected>Open this select menu</option>
                  @foreach ($group as $b)
                  <option value={{$b['id']}} {{$b['id']==$data->group_id ? 'selected':''}}>{{$b['name']}}</option>
                  @endforeach
                </select>
              </div>
              <button type="submit" class="btn btn-primary">Update</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('template.footer')