@include('template.header')

<section id="band">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6 col-lg-4 py-5">
        <div class="card w-100">
          <div class="card-body">
            <h5 class="card-title">Tambah Album</h5>

            <form action="/album" method="post" enctype="multipart/form-data">
              @csrf
              <div class="mb-3">
                <label for="name" class="form-label">Nama Album</label>
                <input type="text" class="form-control" name="name" id="name">
              </div>
              <div class="mb-3">
                <label for="tahun" class="form-label">Tahun</label>
                <input type="number" class="form-control" name="tahun" id="tahun">
              </div>
              <div class="mb-3">
                <label for="harga" class="form-label">Harga</label>
                <input type="number" class="form-control" name="harga" id="harga">
              </div>
              <div class="mb-3">
                <label for="cover" class="form-label">Cover</label>
                <input type="file" class="form-control" name="cover" id="cover">
              </div>
              <div class="mb-3">
                <label for="label" class="form-label">Label</label>
                <select class="form-select" id="label" name="label">
                  <option selected>Open this select menu</option>
                  @foreach ($label as $l)
                  <option value={{$l['id']}}>{{$l['name']}}</option>
                  @endforeach
                </select>
              </div>
              <button type="submit" class="btn btn-success">Tambah</button>
            </form>

          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div class="w-100 overflow-auto">

          <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Tahun</th>
                <th scope="col">Harga</th>
                <th scope="col">Cover</th>
                <th scope="col">Label</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @php
              $i = 0
              @endphp
              @foreach ($data as $d)
              @php
              $i += 1
              @endphp
              <tr>
                <td scope="row">{{$i}}</td>
                <td>{{$d->name}}</td>
                <td>{{$d->year}}</td>
                <td>{{$d->price}}</td>
                <td><img src={{Storage::url('images/'.$d->cover)}} width="50px" height="50px"></td>
                <td>{{$d->label->name}}</td>
                <td>
                  <a href="/album/edit/{{$d->id}}" class="btn btn-info">Edit</a>
                  <a href="/album/delete/{{$d->id}}" class="btn btn-danger">Hapus</a>
                </td>
              </tr>
              @endforeach

            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
</section>
@include('template.footer')