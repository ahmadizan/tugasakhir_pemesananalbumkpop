@include('template.header')
<section>
    <div class="container">
        <div class="row py-5">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h5>Edit Label</h5>
                        <form action="/label/update/{{$data['id']}}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="labelName" class="form-label">Nama</label>
                                <input type="text" value="{{$data['name']}}" class="form-control" name="labelName" id="labelName">
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('template.footer')