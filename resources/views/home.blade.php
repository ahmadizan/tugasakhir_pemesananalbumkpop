@include('template.header')

<section>
    <div class="container">
        <div class="row">
            <div class="col py-5">
                <h1>Selamat Datang, {{Auth::user()->name}}</h1>
                <p class="w-50">Sebagai administrator anda mendapatkan akses penuh untuk memanipulasi data yang dimiliki oleh sistem ini</p>
                <div>
                    <a class="btn btn-info me-4" href="user/edit">Edit Profile</a>
                    <a href="/auth/out" class="text-danger">Logout</a>
                </div>
            </div>
        </div>
    </div>
</section>

@include('template.footer')