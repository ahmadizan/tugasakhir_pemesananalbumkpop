@include('template.header')

<section id="band">
  <div class="container">
    <div class="row py-5">
      <div class="col-12 col-md-6 col-lg-4">

        <div class="card">
          <div class="card-body">
            <h5>Edit Album</h5>
            <form action="/album/update/{{$data->id}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" value={{$data->cover}} name="lastcover">
              <div class="mb-3">
                <label for="name" class="form-label">Nama</label>
                <input type="text" class="form-control" name="name" id="name" value={{$data->name}}>
              </div>
              <div class="mb-3">
                <label for="tahun" class="form-label">Tahun</label>
                <input type="number" class="form-control" name="tahun" id="tahun" value={{$data->year}}>
              </div>
              <div class="mb-3">
                <label for="harga" class="form-label">Harga</label>
                <input type="number" class="form-control" name="harga" id="harga" value={{$data->price}}>
              </div>
              <div class="mb-3">
                <label for="cover" class="form-label">Cover</label>
                <input type="file" class="form-control" name="cover" id="cover">
              </div>
              <div class="mb-3">
                <label for="label" class="form-label">Label</label>
                <select class="form-select" id="label" name="label">
                  @foreach ($label as $l)
                  <option value={{$l['id']}} {{ $data->label_id==$l['id']?'selected':'' }}>{{$l['name']}}</option>
                  @endforeach
                </select>
              </div>
              <button type="submit" class="btn btn-primary">Update</button>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
@include('template.footer')