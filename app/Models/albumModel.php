<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\labelModel;
use App\Models\songModel;

class albumModel extends Model
{

    protected $table = 'albums';
    protected $fillable = [
        'name','year','label_id','price','cover'
    ];

    public function label(){
        return $this->belongsTo(labelModel::class, 'label_id', 'id');
    }

    public function song(){
        return $this->hasMany(songModel::class, 'album_id', 'id');
    }

}
