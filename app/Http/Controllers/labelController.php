<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\labelModel;
use App\Models\albumModel;

class labelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'data' => labelModel::all(),
            'active' => 'label'
        ];
        return view('label',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        labelModel::create([
            'name' => $request->labelName,
        ]);
        return redirect('/label')->with('scs','Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'data' => labelModel::find($id),
            'active' => 'label'
        ];
        return view('editLabel',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        labelModel::where('id', $id)
                ->update(['name' => $request->labelName]);
        return redirect('/label')->with('scs','Data berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if( empty(albumModel::where('label_id',$id)->first()) ){
            labelModel::find($id)->delete();
            return redirect('/label')->with('scs','Data berhasil dihapus');
        }else{
            return redirect('/label')->with('scs','Data masih digunakan pada album');
        }
    }
}
