<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\labelModel;
use App\Models\albumModel;
use App\Models\songModel;
use Illuminate\Support\Facades\Storage;

class albumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'label' => labelModel::select(['id','name'])->get(),
            'data' => albumModel::all(),
            'active' => 'album'
        ];
        // dd($data['publisher'][0]['id']);
        // dd($data['data'][0]->publisher);

        return view('album',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file('cover'));

        // dd(Str::random(10));
        $str = Str::random(10);
        $extension = $request->cover->extension();

        $newName = $str.'.'.$extension;
        // dd($newName);

        $path = $request->cover->storeAs('public/images', $newName);
        
        albumModel::create([
            'name' => $request->name,
            'year' => $request->tahun,
            'label_id' => $request->label,
            'price' => $request->harga,
            'cover' => $newName
        ]);
        return redirect('/album')->with('scs','Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data =[
            'data' => albumModel::find($id),
            'label' => labelModel::select(['id','name'])->get(),
            'active' => 'album'
        ];
        return view('editAlbum',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->cover==null){
            $update = [
                'name' => $request->name,
                'year' => $request->tahun,
                'label_id' => $request->label,
                'price' => $request->harga
            ];
        }else{
            Storage::delete('public/images/'.$request->lastcover);

            $str = Str::random(10);
            $extension = $request->cover->extension();

            $newName = $str.'.'.$extension;
            // dd($newName);

            $path = $request->cover->storeAs('public/images', $newName);

            $update = [
                'name' => $request->name,
                'year' => $request->tahun,
                'label_id' => $request->label,
                'price' => $request->harga,
                'cover' => $newName
            ];
        }
        albumModel::where('id', $id)
                ->update($update);
        return redirect('/album')->with('scs','Data berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(empty(songModel::where('album_id',$id)->first())){

            $img = albumModel::find($id)->cover;
            Storage::delete('public/images/'.$img);
            albumModel::find($id)->delete();
            return redirect('/album')->with('scs','Data berhasil dihapus');
        }else{
            return redirect('/album')->with('scs','Data masih digunakan pada lagu');
        }
    }
}
